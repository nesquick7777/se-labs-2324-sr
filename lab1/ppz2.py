# The exercise comes first (with a few extras if you want the extra challenge or want to spend more time), followed by a discussion. Enjoy!

# Ask the user for a number. Depending on whether the number is even or odd, print out an appropriate message to the user. Hint: how does an even / odd number react differently when divided by 2?

# Extras:

# If the number is a multiple of 4, print out a different message.
# Ask the user for two numbers: one number to check (call it num) and one number to divide by (check). If check divides evenly into num, tell that to the user. If not, print a different appropriate message.


number = int(input("Upišite broj: "))

if number % 4 ==0 and number % 2 == 0 :
    
    print("Broj je dijelji sa 4")
elif number % 2 == 0:
    print("Vaš broj je paran")
else:
    print("Vaš broj je neparan")


num = int(input("Upišite broj koji ćete dijelti: "))

check = int(input("Upišite broj s kojim dijelite: "))

if num % check == 0:
    print("dijeliv je")
else:
    print("nije dijeliv")

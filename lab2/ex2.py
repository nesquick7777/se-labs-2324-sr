import csv

table = []

with open('c:\student\softeng\se-labs-2324-sr\lab2\ex2-text.csv', 'r') as csv_file1:
    reader = csv.reader(csv_file1)
    for row in reader:
        table.append(row)
        
with open('c:\student\softeng\se-labs-2324-sr\lab2\ex2-employees.txt', 'w') as csv_file2:
    writer = csv.writer(csv_file2)
    for _ in table:
        line = "{},{}".format(_[0],_[1])
        writer.writerow([line])
    

with open('c:\student\softeng\se-labs-2324-sr\lab2\ex2-locations.txt', 'w') as csv_file3:
    writer = csv.writer(csv_file3)
    for _ in table:
        writer.writerow(['{},{}'.format(_[0],_[3])])
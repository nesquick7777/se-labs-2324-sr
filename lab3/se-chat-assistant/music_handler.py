from query_handler_base import QueryHandlerBase
import random
import requests
import json

class MusicHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "music" in query:
            return True
        return False

    def process(self, query):
        dates = query.split()
        key1 = dates[1]
        key2 = dates[2]
        

        try:
            result = self.call_api(key1, key2)
            chart = result["content"]
            for _ in chart.values():
                self.ui.say(_)
        except Exception as e:
             print(e)
             self.ui.say("Oh no! There was an error trying to contact Billboard api.")
             self.ui.say("Try something else!")
		
            
    


    def call_api(self, key1, key2):
        url = "https://billboard-api2.p.rapidapi.com/hot-100"

        querystring = {"date":key1,"range":key2}

        headers = {
				"X-RapidAPI-Key": "0b81f2faa2msh19d8a25d6c04e07p1c0b02jsnbbdc0bc96813",
				"X-RapidAPI-Host": "billboard-api2.p.rapidapi.com"
		}

        response = requests.request("GET", url, headers=headers, params=querystring)

        return json.loads(response.text)
from query_handler_base import QueryHandlerBase
import random
import requests
import json

class DadHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "dad" in query:
            return True
        return False

    def process(self,query):
        types = query.split()
        type = types[1]
        
        try:
            result = self.call_api(type)
            setup = result["setup"]
            punchline = result["punchline"]
            self.ui.say(f"{setup}")
            self.ui.say(f"{punchline}")
            
        except Exception as e:
             print(e)
             self.ui.say("Oh no! There was an error trying to contact Dad api.")
             self.ui.say("Try something else!")
		
            
    


    def call_api(self,type):
        url = "https://dad-jokes.p.rapidapi.com/random/joke"
        querystring = {"type":type}


        headers = {
	                "X-RapidAPI-Key": "0b81f2faa2msh19d8a25d6c04e07p1c0b02jsnbbdc0bc96813",
	                "X-RapidAPI-Host": "dad-jokes.p.rapidapi.com"
}

        response = requests.request("GET", url, headers=headers, params=querystring)

        return json.loads(response.text)